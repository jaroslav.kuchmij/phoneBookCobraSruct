package cmd

import (
	"fmt"
	"strings"
	"io/ioutil"
	"github.com/spf13/cobra"
	"gitlab.com/jaroslav.kuchmij/phoneBookCobraSruct.git/utils"
	"gitlab.com/jaroslav.kuchmij/phoneBookCobraSruct.git/models"
)

func init() {
	rootCmd.AddCommand(deleteContactCmd)
}

// Команда удаления контакта по номеру телефона 
var deleteContactCmd = &cobra.Command{
	Use:   "deleteContact",
	Short: "delete contact into file",
	Long:  `This command deleting contact. 
	Searching occurs on phone namber`,
	Run: func(cmd *cobra.Command, args []string) {

	contact := new(models.Contact)

	if utils.CheckIsNotExist() {		//проверка на существование файла
		fmt.Println("File does not exist")
		return
	}
		
	fmt.Println("\t\tUpdating address contact")
 	fmt.Print("Enter the phone number (format: +380981111111):")
	fmt.Scanln(&contact.Namber)
	if !utils.CheckValidNamber(contact.Namber) {		//проверка на формат номера
		fmt.Println("Invalid namber")
		return
	}
	temp := utils.SplitFile()		//разделяем файл на строки (разделитель: пробел)
	for line0, item0 := range temp  {
    	temp1 := strings.Split(item0, "|")		//разделяем строку на значения (разделитель: "|")
    	for line, item := range temp1 {
    		if line == 0 {
    			if utils.CheckSerch(contact.Namber, item) {		//проверка входящего значения на совпадения в файле
    				copy(temp[line0:], temp[line0+1:])  //копируем следующий элемент массива на место найденной строки
				    temp[len(temp)-1] = "" 		//обнуляем "хвост"
             		temp = temp[:len(temp)-1] 		//запись получнного массива в переменную temp
            		output1 := strings.Join(temp, "\n") 	//обьединение строк через разделитель "Enter" 
    				err := ioutil.WriteFile(utils.NameFile, []byte(output1), 0644)		//перезапись существующего файла собранным выше массивом
				        if err != nil {
				                panic(err)
				        }
				    fmt.Println("Deleting phone namber - OK")
				    return
    			}
    		}
    	}
    	
    }

    fmt.Println("This number does not exist")
	},
}