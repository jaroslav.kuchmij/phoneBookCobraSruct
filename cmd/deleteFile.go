package cmd

import (
	"fmt"
	"os"
	"github.com/spf13/cobra"
	"gitlab.com/jaroslav.kuchmij/phoneBookCobraSruct.git/utils"
)

func init() {
	rootCmd.AddCommand(deleteFileCmd)
}

// Команда удаления файла
var deleteFileCmd = &cobra.Command{
	Use:   "delete",
	Short: "delete File",
	Long:  `This command delete a file`,
	Run: func(cmd *cobra.Command, args []string) {
		
	if !utils.CheckIsNotExist() {		//проверка на существование файла
		os.Remove(utils.NameFile)
		fmt.Println("file deleted!")
		return
	}
	fmt.Println("The file does not exist!")
	},
}