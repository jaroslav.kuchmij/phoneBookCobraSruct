package cmd

import (
	"fmt"
	"os"
	"github.com/spf13/cobra"
	"gitlab.com/jaroslav.kuchmij/phoneBookCobraSruct.git/utils"
)

func init() {
	rootCmd.AddCommand(createFileCmd)
}

// 
var createFileCmd = &cobra.Command{
	Use:   "create",
	Short: "create File",
	Long:  `This command creates a file`,
	Run: func(cmd *cobra.Command, args []string) {
		
	if utils.CheckIsNotExist() {		//проверка на существование файла
		file, err := os.Create(utils.NameFile)
		if err != nil {
			return
		}
		defer file.Close()
		fmt.Println("\tCreate file - Done!\n")
		return
	}

	fmt.Println("The file already exists!")
	},
}