package cmd

import (
	"fmt"
	"os"
	"github.com/spf13/cobra"
	"gitlab.com/jaroslav.kuchmij/phoneBookCobraSruct.git/utils"
)

func init() {
	rootCmd.AddCommand(getContactsCmd)
}

// Команда чтения всех элементов из файла
var getContactsCmd = &cobra.Command{
	Use:   "getContacts",
	Short: "get all contacts of file",
	Long:  `This command reads items into file and print this items on the screen`,
	Run: func(cmd *cobra.Command, args []string) {
		
	if !utils.CheckIsNotExist() {		//проверка на существование файла
		file, err := os.Open(utils.NameFile)		//открытие файла
		if err != nil {
			return
		}
		defer file.Close()

		stat, err := file.Stat()		//получение размера файла
		if err != nil {
			return
		}

		bs := make([]byte, stat.Size())		//получение элементов файла
		_, err = file.Read(bs)		//считывание файла
		if err != nil {
			return
		}

		str := string(bs)		//перевод значений в стрингу
		fmt.Println(str)		
		return
	}

	fmt.Println("The file does not exist!")
	},
}