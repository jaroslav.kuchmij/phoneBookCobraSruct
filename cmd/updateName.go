package cmd

import (
	"fmt"
	"strings"
	"io/ioutil"
	"github.com/spf13/cobra"
	"gitlab.com/jaroslav.kuchmij/phoneBookCobraSruct.git/utils"
	"gitlab.com/jaroslav.kuchmij/phoneBookCobraSruct.git/models"
)

func init() {
	rootCmd.AddCommand(updateNameCmd)
}

// Команда изменения имение контакта по номеру телефона 
var updateNameCmd = &cobra.Command{
	Use:   "updateName",
	Short: "update Name into file",
	Long:  `This command updating name contact. 
	Searching occurs on phone namber`,
	Run: func(cmd *cobra.Command, args []string) {

	contact := new(models.Contact)

	if utils.CheckIsNotExist() {		//проверка на существование файла
		fmt.Println("File does not exist")
		return
	}
		
	fmt.Println("\t\tUpdating name contract")
 	fmt.Print("Enter the phone number (format: +380981111111):")
	fmt.Scanln(&contact.Namber)
	if !utils.CheckValidNamber(contact.Namber) { 	//проверка на формат номера
		fmt.Println("Invalid namber")
		return
	}

	if bool, _ := utils.SearchNamberInFile(contact.Namber); bool == false {		//проверка на существование номера в файле
		fmt.Println("This number does not exist")
		return
	}

	temp := utils.SplitFile()		//разделяем файл на строки (разделитель: пробел)
	for line0, item0 := range temp  {
    	temp1 := strings.Split(item0, "|")		//разделяем строку на значения (разделитель: "|")
    	for line, item := range temp1 {
    		if line == 0 {
    			if utils.CheckSerch(contact.Namber, item) {		//проверка входящего значения на совпадения в файле
    				fmt.Println("Enter the new Name")
    				fmt.Scanln(&contact.Name)
    				temp1[1] = contact.Name		//запись нового значения в поле Name
    				output := strings.Join(temp1, "|")		//обьединение значений через разделитель "|"
    				temp[line0] = output 		//перезапись строки на верхнем уровне
    				output1 := strings.Join(temp, "\n")		//обьединение строк через разделитель "Enter" 
    				err := ioutil.WriteFile(utils.NameFile, []byte(output1), 0644)		//перезапись существующего файла собранным выше массивом
				        if err != nil {
				                panic(err)
				        }
				    fmt.Println("Updating phone namber - OK")
				    return
    			}
    		}
    	}
	}
	
	},
}