package cmd

import (
	"fmt"
	"os"
	"github.com/spf13/cobra"
	"gitlab.com/jaroslav.kuchmij/phoneBookCobraSruct.git/utils"
	"gitlab.com/jaroslav.kuchmij/phoneBookCobraSruct.git/models"
)

func init() {
	rootCmd.AddCommand(addContactCmd)
}

//Внесение в справочник номера, ФИО, адреса контакта
var addContactCmd = &cobra.Command{
	Use:   "addContact",
	Short: "add contact from file",
	Long:  `This command add phone namber, name and address into file.

	Each element is divided "|"
	Format phone: +380111111111
	Format Name and Address: free form

	Example: +380984001650|Yaroslav|Voenomorskaya str.`,
	Run: func(cmd *cobra.Command, args []string) {

	contact := new(models.Contact)

	fmt.Println("Enter the phone number (format: +380981111111):")
	fmt.Scanln(&contact.Namber)

	if !utils.CheckIsNotExist() {		//проверка на существование файла
		if !utils.CheckValidNamber(contact.Namber) {		//проверка на формат номера
			fmt.Println("Invalid namber")
			return
		}

		if bool, value := utils.SearchNamberInFile(contact.Namber); bool == true {		//проверка на существование номера в файле
			fmt.Println("This number exists")
			fmt.Println(value)
			return
		}
	}

	fmt.Println("Enter Name:")
	fmt.Scanln(&contact.Name)

	fmt.Println("Enter address:")
	fmt.Scanln(&contact.Address)

	setContact(contact)		//запись введенных данных в файл

	fmt.Println("Contact added!!!")
	},
}

func setContact(contact *models.Contact) {
	f, err := os.OpenFile(utils.NameFile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0600)		//открытие файла. Если нет, тогда создаем новый
		if err != nil {
			panic(err)
		}
		defer f.Close()

		//запись входящего параметра в файл и установка разделителя "|"
		if _, err = f.WriteString(string(contact.Namber)+"|"+string(contact.Name)+"|"+string(contact.Address)+"\n"); err != nil {
			panic(err)
		}
}

