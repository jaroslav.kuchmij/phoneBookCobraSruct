# Project "PhoneBook on Cobra"

## Данный репозиторий содержит самостоятельный проект на языке GO 
 
### Условия задачи:
1. Перенести проект "Телефонная книга" на "COBRA"
2. Добавить новое поле "Адрес"
3. Вынести телефонную книгу в структуру

### Инструкция по создания проекта:
1. Установить cobra - открыл консоль cmd и ввел
<pre>
go get github.com/spf13/cobra/cobra
</pre>

2. Создать свое приложение, для этого перейти в консоле в директорию %GOPATH%\bin и набрать команду 
<pre>
<pre>
cobra init %GOPATH%/[Путь к проекту] (Например: E:\golang\project\src\gitlab.com\jaroslav.kuchmij\phoneBookCobraSruct.git)
</pre>
команда создаст исходный код приложения. Это очень мощное приложение, которое заполнит программу правильной структурой, 
чтобы можно было сразу пользоваться всеми преимуществами Cobra.

3. После инициализации приложения Cobra, создать в пакете cmd(gitlab.com\jaroslav.kuchmij\phoneBookCobraSruct.git/cmd)
необходимые файлы с командами.
Для имен команд использывать camelCase (не snake_case / snake-case). В противном случае будут ошибки. Например, cobra add add-user неверен, но cobra add addUser верно.

4. Все вспомогательные функции вынести в пакет utils(gitlab.com\jaroslav.kuchmij\phoneBookCobraSruct.git/utils). 
5. Структуру контакта вынести в пакет models(gitlab.com\jaroslav.kuchmij\phoneBookCobraSruct.git/models).
6. Из директории gitlab.com\jaroslav.kuchmij\phoneBookCobraSruct.git , проверяем работоспособность приложения. Для этого запускаем следующее:
<pre>
go run main.go [command]
</pre>
этой командой выполняем логику прописанную в файле с коммандой

### Запуск проекта:
1. Скачать проект - 
<pre>
go get gitlab.com/jaroslav.kuchmij/phoneBookCobraSruct.git
</pre>
2. Перейти в терминале в папку  %GOPATH%/src/gitlab.com/jaroslav.kuchmij/phoneBookCobraSruct.git
3. Ввыполнить команду go run main.go <command>:
    - create - создание файла
    - delete - удаление файла
    - addContact - добавление контакта
    - searchContact - поиск контакта по номеру телефона
    - getContacts - получение всего списка контактов
    - updateName - изменение имени по телефону
    - updateAddress - изменение адреса по номеру телефона
    - deleteContact - удаление контакта по номеру телефона

**~~Создатель кода~~** Главный бездельник - [*YaroslavKuchmiy*](https://www.facebook.com/profile.php?id=100006520172879)