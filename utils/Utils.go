package utils

import (
	"os"
	"regexp"
	"io/ioutil"
	"strings"
)

var NameFile = "test.txt"

//Поиск совпадений по номеру в файле
func SearchNamberInFile(incomingValue string) (bool, string) {
	temp := SplitFile()    //разделяем файл на строки (разделитель: пробел)
	for _, item0 := range temp  {
		temp1 := strings.Split(item0, "|")		//разделяем строку на значения (разделитель: "|")
		for line, item := range temp1 {
			if line == 0 {
				if CheckSerch(incomingValue, item) {		//проверка входящего значения на совпадения в файле
					return true, item0
				}
			}
		}

	}
	return false, ""
}

//Проверка на существование файла
func CheckIsNotExist() bool {
	var _, err = os.Stat(NameFile)
	return os.IsNotExist(err)
}

//Проверка входящего номера на корректность
func CheckValidNamber(incomingValue string) bool {
	var validID = regexp.MustCompile(`^[+][3][8][0][0-9\\s\\(\\)\\+]{9}$`) //формат номера
	res := validID.MatchString(incomingValue)
	return res
}

//Проверка входящего значения на совпадение с проверочным значением при поиске
func CheckSerch(incomingValue string, checkValue string) bool {
	res := strings.Contains(checkValue, incomingValue)
	return res
}

//Разбивка файла на строки
func SplitFile() []string {
	/* ioutil.ReadFile returns []byte, error */
	data, err := ioutil.ReadFile(NameFile)
	if err != nil {
		panic(err)
	}
	/* ... omitted error check..and please add ... */
	/* find index of newline */
	file := string(data)
	/* func Split(s, sep string) []string */
	res := strings.Split(file, "\n")
	return res
}